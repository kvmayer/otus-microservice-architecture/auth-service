package controller

import (
	"github.com/gin-gonic/gin"
	"mayerkv/auth/domain"
	"net/http"
	"strconv"
)

type AuthController struct {
	authService *domain.AuthService
}

func NewAuthController(authService *domain.AuthService) *AuthController {
	return &AuthController{authService: authService}
}

func (c *AuthController) SignUp(ctx *gin.Context) {
	var dto domain.SignUpDto
	if err := ctx.ShouldBindJSON(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	_, err := c.authService.Register(&dto)
	if err != nil {
		handleError(ctx, err)
		return
	}

	ctx.AbortWithStatus(http.StatusCreated)
}

func (c *AuthController) SignIn(ctx *gin.Context) {
	var dto domain.SignInDto
	if err := ctx.ShouldBindJSON(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	token, err := c.authService.SignIn(&dto)
	if err != nil {
		handleError(ctx, err)
		return
	}

	ctx.AbortWithStatusJSON(http.StatusOK, token)
}

func (c *AuthController) Authorize(ctx *gin.Context) {
	if claims, ok := ctx.Get("claims"); !ok {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	} else {
		customClaims := claims.(*domain.CustomClaims)
		ctx.Header("x-user-id", strconv.Itoa(customClaims.UserId))
		ctx.AbortWithStatus(http.StatusOK)
	}
}

func (c *AuthController) Refresh(ctx *gin.Context) {
	var dto domain.RefreshTokenDto
	if err := ctx.ShouldBindJSON(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	token, err := c.authService.Refresh(&dto)
	if err != nil {
		handleError(ctx, err)
		return
	}

	ctx.AbortWithStatusJSON(http.StatusOK, token)
}
