package controller

import (
	"github.com/gin-gonic/gin"
	"mayerkv/auth/domain"
	"net/http"
)

type UserDto struct {
	Id      int    `json:"id"`
	Name    string `json:"name"`
	Surname string `json:"surname"`
	Login   string `json:"login"`
}

type UserController struct {
	userService *domain.UserService
}

func NewUserController(userService *domain.UserService) *UserController {
	return &UserController{userService: userService}
}

func (c *UserController) GetUser(ctx *gin.Context) {
	var dto domain.GetUserDto
	if err := ctx.ShouldBindUri(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	user, err := c.userService.GetUser(&dto)
	if err != nil {
		handleError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, &UserDto{
		Id:      user.Id,
		Name:    user.Name,
		Surname: user.Surname,
		Login:   user.Login,
	})
}

func (c *UserController) UpdateUser(ctx *gin.Context) {

}

func (c *UserController) Me(ctx *gin.Context) {
	userId := ctx.GetInt("user-id")

	user, err := c.userService.GetUser(&domain.GetUserDto{Id: userId})
	if err != nil {
		handleError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, &UserDto{
		Id:      user.Id,
		Name:    user.Name,
		Surname: user.Surname,
		Login:   user.Login,
	})
}
