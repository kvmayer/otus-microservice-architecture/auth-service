package controller

import (
	"errors"
	"github.com/gin-gonic/gin"
	"mayerkv/auth/domain"
	"net/http"
	"strings"
)

var (
	UnsupportedMediaType = errors.New("unsupported media type")
	InvalidAuthHeader    = errors.New("invalid authorization header")
)

func MediaType() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if header := ctx.GetHeader("content-type"); header != "application/json" {
			handleError(ctx, UnsupportedMediaType)
			return
		}
		ctx.Next()
	}
}

func Auth(tokenService *domain.AuthTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		tokenHeader := ctx.GetHeader("authorization")
		if !strings.HasPrefix(tokenHeader, "Bearer ") {
			handleError(ctx, InvalidAuthHeader)
			return
		}

		tokenString := tokenHeader[7:]
		token, err := tokenService.Decode(tokenString)
		if err != nil || !token.Valid {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		claims, _ := token.Claims.(*domain.CustomClaims)
		if claims.Type != domain.ClaimTypeAccess {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		ctx.Set("claims", claims)
		ctx.Set("user-id", claims.UserId)
		ctx.Next()
	}
}
