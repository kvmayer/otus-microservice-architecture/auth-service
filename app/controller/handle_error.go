package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt"
	"mayerkv/auth/domain"
	"mayerkv/auth/infrastructure"
	"net/http"
	"strconv"
)

func handleError(ctx *gin.Context, err error) {
	code, message := http.StatusInternalServerError, "internal server error"

	switch err {
	case domain.UserAlreadyExists, domain.UserNotExists:
		code = http.StatusBadRequest
		message = err.Error()
	case UnsupportedMediaType:
		code = http.StatusUnsupportedMediaType
		message = err.Error()
	case domain.UnsupportedTokenType, domain.InvalidPassword, domain.InvalidRefreshToken, InvalidAuthHeader, domain.UserNotActive:
		code = http.StatusUnauthorized
		message = err.Error()
	}

	switch err.(type) {
	case validator.ValidationErrors, *infrastructure.InvalidArgument:
		code = http.StatusBadRequest
		message = err.Error()
	case *jwt.ValidationError:
		code = http.StatusUnauthorized
		message = err.Error()
	case *strconv.NumError:
		code = http.StatusBadRequest
		message = err.Error()
	}

	ctx.AbortWithStatusJSON(code, gin.H{
		"error": message,
	})
}
