package di

import (
	"go.uber.org/dig"
	"mayerkv/auth/infrastructure"
)

func ProvideRepository(container *dig.Container) error {
	if err := container.Provide(infrastructure.NewInMemoryUserRepository); err != nil {
		return err
	}
	return nil
}
