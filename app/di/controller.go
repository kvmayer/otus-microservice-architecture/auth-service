package di

import (
	"go.uber.org/dig"
	"mayerkv/auth/app/controller"
)

func ProvideController(container *dig.Container) error {
	if err := container.Provide(controller.NewAuthController); err != nil {
		return err
	}
	if err := container.Provide(controller.NewUserController); err != nil {
		return err
	}
	return nil
}
