package di

import (
	"crypto/rsa"
	"errors"
	"github.com/golang-jwt/jwt"
	"go.uber.org/dig"
	"mayerkv/auth/app/common"
	"mayerkv/auth/domain"
	"mayerkv/auth/infrastructure"
)

func ProvideService(container *dig.Container) error {
	if err := container.Provide(domain.NewUserService); err != nil {
		return err
	}
	if err := container.Provide(domain.NewAuthService); err != nil {
		return err
	}
	if err := container.Provide(domain.NewAuthTokenService); err != nil {
		return err
	}

	if err := container.Provide(getRsaKey); err != nil {
		return err
	}
	if err := container.Provide(infrastructure.NewBCryptPasswordEncoder); err != nil {
		return err
	}
	return nil
}

func getRsaKey(config *common.Config) (*rsa.PrivateKey, error) {
	if config.RsaPrivateKey == "" {
		return nil, errors.New("private key not present")
	}

	return jwt.ParseRSAPrivateKeyFromPEM([]byte(config.RsaPrivateKey))
}
