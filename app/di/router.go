package di

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/dig"
	"mayerkv/auth/app/controller"
	"mayerkv/auth/domain"
	"net/http"
)

func ProvideRouter(container *dig.Container) error {
	if err := container.Provide(createRouter); err != nil {
		return err
	}
	return nil
}


func createRouter(tokenService *domain.AuthTokenService, authController *controller.AuthController, userController *controller.UserController) *gin.Engine {
	authMiddleware := controller.Auth(tokenService)
	mediaTypeMiddleware := controller.MediaType()

	router := gin.Default()

	auth := router.Group("/auth", mediaTypeMiddleware)
	{
		auth.POST("/sign-in", authController.SignIn)
		auth.POST("/sign-up", authController.SignUp)
		auth.GET("/authorize", authMiddleware, authController.Authorize)
		auth.POST("/refresh", authController.Refresh)
	}

	users := router.Group("/users", mediaTypeMiddleware)
	{
		users.Use(authMiddleware)

		users.GET("/me", userController.Me)
		users.GET("/:id", userController.GetUser)
		users.PATCH("/:id", userController.UpdateUser)
	}

	router.GET("/health", func(context *gin.Context) {
		context.Status(http.StatusOK)
	})

	return router
}