package di

import (
	"go.uber.org/dig"
	"mayerkv/auth/app/common"
)

func ProvideConfig(container *dig.Container) error {
	if err := container.Provide(common.ConfigFromEnv); err != nil {
		return err
	}

	return nil
}
