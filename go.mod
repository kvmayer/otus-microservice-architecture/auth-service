module mayerkv/auth

go 1.16

require (
	github.com/Masterminds/squirrel v1.5.0
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/uuid v1.3.0
	github.com/jackc/pgconn v1.10.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/jmoiron/sqlx v1.3.4
	go.uber.org/dig v1.12.0
	golang.org/x/crypto v0.0.0-20210915214749-c084706c2272
	golang.org/x/text v0.3.7 // indirect
)
