package domain

type GetUserDto struct {
	Id int `json:"id" uri:"id" binding:"required,numeric"`
}
