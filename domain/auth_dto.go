package domain

type SignUpDto struct {
	Name     string `json:"name" binding:"required,alpha"`
	Surname  string `json:"surname" binding:"required,alpha"`
	Login    string `json:"login" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type SignInDto struct {
	Login    string `json:"login" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type RefreshTokenDto struct {
	RefreshToken string `json:"refresh_token" binding:"required"`
}
