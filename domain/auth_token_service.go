package domain

import (
	"crypto/rsa"
	"errors"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"time"
)

var (
	UserNotActive = errors.New("user not active")
)

type AuthTokenService struct {
	privateKey *rsa.PrivateKey
	UserRepository
}

func NewAuthTokenService(privateKey *rsa.PrivateKey) *AuthTokenService {
	return &AuthTokenService{
		privateKey: privateKey,
	}
}

func (s *AuthTokenService) PublicKey() *rsa.PublicKey {
	return &s.privateKey.PublicKey
}

func (s *AuthTokenService) GenerateAuthToken(user *User) (*AuthToken, error) {
	if !user.Active {
		return nil, UserNotActive
	}

	now := time.Now()
	accessToken := jwt.NewWithClaims(jwt.SigningMethodRS256, &CustomClaims{
		user.Id,
		ClaimTypeAccess,
		jwt.StandardClaims{
			Id:        uuid.NewString(),
			Subject:   user.Login,
			Issuer:    "auth-service",
			IssuedAt:  now.Unix(),
			ExpiresAt: now.Add(15 * time.Minute).Unix(),
		},
	})
	refreshToken := jwt.NewWithClaims(jwt.SigningMethodRS256, &CustomClaims{
		user.Id,
		ClaimTypeRefresh,
		jwt.StandardClaims{
			Id:        uuid.NewString(),
			Subject:   user.Login,
			Issuer:    "auth-service",
			IssuedAt:  now.Unix(),
			ExpiresAt: now.Add(1 * time.Hour).Unix(),
		},
	})
	accessTokenString, _ := accessToken.SignedString(s.privateKey)
	refreshTokenString, _ := refreshToken.SignedString(s.privateKey)

	return &AuthToken{
		AccessToken:  accessTokenString,
		RefreshToken: refreshTokenString,
	}, nil
}

func (s *AuthTokenService) Decode(tokenString string) (*jwt.Token, error) {
	return jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return s.privateKey.Public(), nil
	})
}

func (s *AuthTokenService) GenerateFromRefresh(refreshToken *jwt.Token) (*AuthToken, error) {
	claims, _ := refreshToken.Claims.(*CustomClaims)
	user, err := s.UserRepository.FindById(claims.UserId)
	if err != nil {
		return nil, err
	}

	return s.GenerateAuthToken(user)
}
