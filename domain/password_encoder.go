package domain

type PasswordEncoder interface {
	Encode(password string) string
	CheckPasswordHash(password, hashedPassword string) bool
}
