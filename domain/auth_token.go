package domain

import "github.com/golang-jwt/jwt"

type AuthToken struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type ClaimType int

const (
	ClaimTypeAccess = iota + 1
	ClaimTypeRefresh
)

type CustomClaims struct {
	UserId int       `json:"uid"`
	Type   ClaimType `json:"ttp"`
	jwt.StandardClaims
}
