package domain

type UserRepository interface {
	NextId() (int, error)
	Save(user *User) error
	FindByUsername(username string) (*User, error)
	FindById(id int) (*User, error)
}
