package domain

type Credentials struct {
	Username string
	Password string
}

type User struct {
	Id          int
	Name        string
	Surname     string
	Login       string
	Active      bool
	Credentials Credentials
}

func SingUp(id int, dto *SignUpDto, encoder PasswordEncoder) *User {
	return &User{
		Id:      id,
		Name:    dto.Name,
		Surname: dto.Surname,
		Login:   dto.Login,
		Active:  true,
		Credentials: Credentials{
			Username: dto.Login,
			Password: encoder.Encode(dto.Password),
		},
	}
}
