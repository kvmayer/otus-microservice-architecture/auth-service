package domain

import "errors"

var (
	UserAlreadyExists = errors.New("user already exists")
	UserNotExists     = errors.New("user not exists")
)

type UserService struct {
	userRepository UserRepository
}

func NewUserService(userRepository UserRepository) *UserService {
	return &UserService{userRepository: userRepository}
}

func (s *UserService) GetUser(dto *GetUserDto) (*User, error) {
	user, err := s.userRepository.FindById(dto.Id)
	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, UserNotExists
	}

	return user, nil
}
