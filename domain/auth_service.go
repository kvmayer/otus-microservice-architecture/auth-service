package domain

import "errors"

var (
	InvalidPassword      = errors.New("invalid password")
	InvalidRefreshToken  = errors.New("invalid refresh token")
	UnsupportedTokenType = errors.New("unsupported token type")
)

type AuthService struct {
	userRepository   UserRepository
	passwordEncoder  PasswordEncoder
	authTokenService *AuthTokenService
}

func NewAuthService(userRepository UserRepository, passwordEncoder PasswordEncoder, authTokenService *AuthTokenService) *AuthService {
	return &AuthService{userRepository: userRepository, passwordEncoder: passwordEncoder, authTokenService: authTokenService}
}

func (s *AuthService) Register(dto *SignUpDto) (*User, error) {
	if user, err := s.userRepository.FindByUsername(dto.Login); err != nil {
		return nil, err
	} else if user != nil {
		return nil, UserAlreadyExists
	}

	id, err := s.userRepository.NextId()
	if err != nil {
		return nil, err
	}

	user := SingUp(id, dto, s.passwordEncoder)

	if err := s.userRepository.Save(user); err != nil {
		return nil, err
	}

	return user, nil
}

func (s *AuthService) SignIn(dto *SignInDto) (*AuthToken, error) {
	user, err := s.userRepository.FindByUsername(dto.Login)
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, UserNotExists
	}

	if !s.passwordEncoder.CheckPasswordHash(dto.Password, user.Credentials.Password) {
		return nil, InvalidPassword
	}

	return s.authTokenService.GenerateAuthToken(user)
}

func (s *AuthService) Refresh(dto *RefreshTokenDto) (*AuthToken, error) {
	token, err := s.authTokenService.Decode(dto.RefreshToken)
	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, InvalidRefreshToken
	}

	customClaims := token.Claims.(*CustomClaims)
	if customClaims.Type != ClaimTypeRefresh {
		return nil, UnsupportedTokenType
	}

	return s.authTokenService.GenerateFromRefresh(token)
}
