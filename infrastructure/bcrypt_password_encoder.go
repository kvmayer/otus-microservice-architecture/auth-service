package infrastructure

import (
	"golang.org/x/crypto/bcrypt"
	"mayerkv/auth/domain"
)

type BCryptPasswordEncoder struct {
}

func NewBCryptPasswordEncoder() domain.PasswordEncoder {
	return &BCryptPasswordEncoder{}
}

func (e *BCryptPasswordEncoder) Encode(password string) string {
	pass, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	return string(pass)
}

func (e *BCryptPasswordEncoder) CheckPasswordHash(password, hashedPassword string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)) == nil
}
