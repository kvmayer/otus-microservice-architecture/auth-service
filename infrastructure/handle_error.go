package infrastructure

import "github.com/jackc/pgconn"

func handlerError(err error) error {
	if pqError, ok := err.(*pgconn.PgError); ok {
		return handleDriverError(pqError)
	}

	return err
}

func handleDriverError(err *pgconn.PgError) error {
	switch err.Code {
	case "23505":
		return NewInvalidArgument(err.Message)
	default:
		return err
	}
}

type InvalidArgument struct {
	msg string
}

func NewInvalidArgument(msg string) *InvalidArgument {
	return &InvalidArgument{msg: msg}
}

func (e *InvalidArgument) Error() string {
	return e.msg
}
