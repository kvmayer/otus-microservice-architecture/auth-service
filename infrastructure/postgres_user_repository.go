package infrastructure

import (
	"context"
	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"mayerkv/auth/domain"
	"time"
)

type PostgresUserRepository struct {
	db *sqlx.DB
}

func NewPostgresUserRepository(db *sqlx.DB) domain.UserRepository {
	return &PostgresUserRepository{db: db}
}

func (r *PostgresUserRepository) NextId() (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	var id int
	query := "select nextval('users_id_seq')"
	err := r.db.GetContext(ctx, &id, query)
	if err != nil {
		return 0, handlerError(err)
	}

	return id, nil
}

func (r *PostgresUserRepository) Save(user *domain.User) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	tx, err := r.db.Beginx()
	if err != nil {
		return handlerError(err)
	}
	defer func() {
		if r := recover(); r != nil {
			_ = tx.Rollback()
		}
	}()

	userQuery, args, err := squirrel.
		Insert("users").
		Columns("id", "name", "surname", "login", "active").
		Values(user.Id, user.Name, user.Surname, user.Login, user.Active).
		Suffix("on conflict (id) do update set name = excluded.name, surname = excluded.surname, login = excluded.login, active = excluded.active").
		PlaceholderFormat(squirrel.Dollar).
		ToSql()
	if err != nil {
		return handlerError(err)
	}

	if _, err = tx.ExecContext(ctx, userQuery, args...); err != nil {
		return handlerError(err)
	}

	credentialsQuery, args, err := squirrel.
		Insert("credentials").
		Columns("user_id", "username", "password").
		Values(user.Credentials.Username, user.Credentials.Password).
		Suffix("on conflict (user_id) do update set username = excluded.username, password = excluded.passwors").
		PlaceholderFormat(squirrel.Dollar).
		ToSql()
	if err != nil {
		return handlerError(err)
	}

	if _, err = tx.ExecContext(ctx, credentialsQuery, args...); err != nil {
		return handlerError(err)
	}

	return nil
}

func (r *PostgresUserRepository) FindByUsername(username string) (*domain.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	var user domain.User
	query, args, err := squirrel.
		Select("id", "name", "surname", "login", "active").
		From("users").
		Where(squirrel.Eq{"login": username}).
		Limit(1).
		PlaceholderFormat(squirrel.Dollar).
		ToSql()
	if err != nil {
		return nil, handlerError(err)
	}
	if err := r.db.GetContext(ctx, &user, query, args...); err != nil {
		return nil, handlerError(err)
	}

	if err := squirrel.
		Select("username", "password").
		From("credentials").
		Where(squirrel.Eq{"user_id": user.Id}).
		Limit(1).
		PlaceholderFormat(squirrel.Dollar).
		Scan(&user.Credentials.Username, user.Credentials.Password); err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *PostgresUserRepository) FindById(id int) (*domain.User, error) {
	panic("not implemented")
}
