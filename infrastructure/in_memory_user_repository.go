package infrastructure

import (
	"mayerkv/auth/domain"
	"sync"
)

type sequence struct {
	value int
}

func (s *sequence) next() int {
	s.value += 1

	return s.value
}

type InMemoryUserRepository struct {
	users map[int]domain.User
	mu    *sync.Mutex
	seq   *sequence
}

func NewInMemoryUserRepository() domain.UserRepository {
	return &InMemoryUserRepository{
		users: map[int]domain.User{},
		mu:    &sync.Mutex{},
		seq:   &sequence{value: 0},
	}
}

func (r *InMemoryUserRepository) NextId() (int, error) {
	return r.seq.next(), nil
}

func (r *InMemoryUserRepository) Save(user *domain.User) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.users[user.Id] = *user

	return nil
}

func (r *InMemoryUserRepository) FindByUsername(username string) (*domain.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	for _, user := range r.users {
		if user.Login == username {
			return &user, nil
		}
	}

	return nil, nil
}

func (r *InMemoryUserRepository) FindById(id int) (*domain.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	user, ok := r.users[id]
	if ok {
		return &user, nil
	}

	return nil, nil
}
