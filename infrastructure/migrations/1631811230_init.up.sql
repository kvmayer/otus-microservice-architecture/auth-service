create table if not exists users
(
    id      int primary key not null,
    name    varchar(50)     not null,
    surname varchar(50)     not null,
    login   varchar(50)     not null unique,
    active  bool            not null
);

create sequence if not exists users_id_seq owned by users.id;

create table if not exists credentials
(
    user_id  int         not null unique,
    username varchar(50) not null,
    password varchar     not null,
    constraint fk_users foreign key (user_id) references users (id) on DELETE cascade
);
