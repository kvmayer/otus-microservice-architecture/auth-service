package main

import (
	"context"
	"embed"
	"errors"
	"flag"
	"github.com/gin-gonic/gin"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/httpfs"
	"github.com/jmoiron/sqlx"
	"go.uber.org/dig"
	"log"
	"mayerkv/auth/app/common"
	"mayerkv/auth/app/di"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"syscall"
	"time"
)

//go:embed infrastructure/migrations/*.sql
var migrationFS embed.FS
var httpFlag bool
var migrateFlag string

func main() {
	flag.StringVar(&migrateFlag, "migrate", "", "up|down|latest")
	flag.BoolVar(&httpFlag, "http", false, "Run http server")
	flag.Parse()

	if !httpFlag && migrateFlag == "" {
		flag.Usage()
		return
	}

	providers := []func(container *dig.Container) error{
		di.ProvideConfig,
		di.ProvideController,
		di.ProvideDatabase,
		di.ProvideRepository,
		di.ProvideRouter,
		di.ProvideService,
	}
	container := dig.New()
	for _, provide := range providers {
		if err := provide(container); err != nil {
			log.Fatal(err)
		}
	}

	if migrateFlag != "" {
		if err := container.Invoke(runMigrations); err != nil {
			log.Fatal(err)
		}
	}

	if httpFlag {
		if err := container.Invoke(runServer); err != nil {
			log.Fatal(err)
		}
	}
}

func runServer(router *gin.Engine, config *common.Config) {
	srv := &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(config.GracefulDelay)*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	select {
	case <-ctx.Done():
		log.Printf("timeout of %d seconds.", config.GracefulDelay)
	}
	log.Println("Server exiting")
}

func runMigrations(db *sqlx.DB) error {
	if ok, err := regexp.Match("^up|down|latest$", []byte(migrateFlag)); err != nil || !ok {
		return errors.New("invalid migration option")
	}

	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		return err
	}

	fs := http.FS(migrationFS)
	s, err := httpfs.New(fs, "infrastructure/migrations")
	if err != nil {
		return err
	}

	instance, err := migrate.NewWithInstance("httpfs", s, "pg", driver)
	if err != nil {
		return err
	}

	switch migrateFlag {
	case "up":
		err = instance.Steps(1)
	case "down":
		err = instance.Steps(-1)
	case "latest":
		err = instance.Up()
	}

	if err != migrate.ErrNoChange {
		return err
	}

	return nil
}
