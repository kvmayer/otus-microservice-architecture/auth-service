FROM golang:1.16-alpine3.14 as builder
WORKDIR /src
COPY . .
RUN go mod tidy
RUN go build -o auth-service

FROM alpine:3.14
WORKDIR /app
COPY --from=builder /src/auth-service .
CMD ["/app/auth-service"]